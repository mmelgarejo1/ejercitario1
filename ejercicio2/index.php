<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="estilos.css">
    <title>Ejercicio Tabla</title>
</head>
<body>
    <?php
        $cabecera = "Productos";
        $titulo1 = "Nombre";
        $titulo2 = "Cantidad";
        $titulo3 = "Precio (Gs)";
        $productos = array("Coca Cola", "Pepsi", "Sprite", "Guarana", "SevenUp", "Mirinda Naranja", "Mirinda Guarana", "Fanta Naranja", "Fanta Piña");
        $cantidades = array(100, 30, 20, 200, 24, 56, 89, 10, 2);
        $precios = array(4.500, 4.800, 4.500, 4.500, 4.800, 4.800, 4.800, 4.500, 4.500);
        echo "<table>";
        echo '  <tr class="cabecera">';
        echo '      <td colspan="3">' .$cabecera. '</td>';
        echo '  </tr>';
        echo '  <tr class="titulos">';
        echo '      <td>' .$titulo1. '</td>';
        echo '      <td>' .$titulo2. '</td>';
        echo '      <td>' .$titulo3. '</td>';
        echo '  <tr class = "inpar">';
        echo '      <td>' .$productos[0]. '</td>';
        echo '      <td>' .$cantidades[0]. '</td>';
        echo '      <td>' .number_format($precios[0], 3). '</td>';
        echo '  <tr class = "par">';
        echo '      <td>' .$productos[1]. '</td>';
        echo '      <td>' .$cantidades[1]. '</td>';
        echo '      <td>' .number_format($precios[1], 3). '</td>';
        echo '  <tr class = "inpar">';
        echo '      <td>' .$productos[2]. '</td>';
        echo '      <td>' .$cantidades[2]. '</td>';
        echo '      <td>' .number_format($precios[2], 3). '</td>';
        echo '  <tr class = "par">';
        echo '      <td>' .$productos[3]. '</td>';
        echo '      <td>' .$cantidades[3]. '</td>';
        echo '      <td>' .number_format($precios[3], 3). '</td>';
        echo '  <tr class = "inpar">';
        echo '      <td>' .$productos[4]. '</td>';
        echo '      <td>' .$cantidades[4]. '</td>';
        echo '      <td>' .number_format($precios[4], 3). '</td>';
        echo '  <tr class = "par">';
        echo '      <td>' .$productos[5]. '</td>';
        echo '      <td>' .$cantidades[5]. '</td>';
        echo '      <td>' .number_format($precios[5], 3). '</td>';
        echo '  <tr class = "inpar">';
        echo '      <td>' .$productos[6]. '</td>';
        echo '      <td>' .$cantidades[6]. '</td>';
        echo '      <td>' .number_format($precios[6], 3). '</td>';
        echo '  <tr class = "par">';
        echo '      <td>' .$productos[7]. '</td>';
        echo '      <td>' .$cantidades[7]. '</td>';
        echo '      <td>' .number_format($precios[7], 3). '</td>';
        echo '  <tr class = "inpar">';
        echo '      <td>' .$productos[8]. '</td>';
        echo '      <td>' .$cantidades[8]. '</td>';
        echo '      <td>' .number_format($precios[8], 3). '</td>';
        echo '</table>';
    ?>
</body>
</html> 